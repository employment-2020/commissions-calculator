![coverage](https://gitlab.com/employment-2020/commissions-calculator/badges/master/coverage.svg)

# Scripts

Install dependencies

```
npm install
```

Start the application in development mode and watch for file changes

```
npm start
```

Build application

```
npm build
```

Start application

```
node dist/app.js input.json
```

Test application, output coverage information and watch for file changes

```
npm test
```

Test application and output coverage information

```
npm run test-prod
```

Check for lint errors

```
npm run lint
```
