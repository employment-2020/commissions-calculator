const fs = require('fs');
const path = require('path');
const Dotenv = require('dotenv-webpack');
const NodemonPlugin = require('nodemon-webpack-plugin');

module.exports = function (env, argv) {
  const isProduction = argv.mode === 'production';

  return {
    target: 'node',
    entry: './src/index.ts',
    output: {
      filename: 'app.js',
      path: path.resolve(__dirname, 'dist'),
    },
    devtool: isProduction ? false : 'cheap-source-map',
    plugins: [
      new Dotenv({
        path:
          !isProduction && !fs.existsSync('./.env')
            ? './.env.development'
            : './.env',
      }),
      !isProduction &&
        new NodemonPlugin({
          args: [env.scriptArguments],
        }),
    ].filter(Boolean),
    resolve: {
      modules: ['src', 'node_modules'],
      extensions: ['.js', '.ts'],
    },
    module: {
      rules: [
        {
          test: /\.(js|ts)$/,
          use: 'babel-loader',
          exclude: /node_modules/,
        },
      ],
    },
  };
};
