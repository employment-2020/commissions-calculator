import { printCommissions } from 'output/commission';
import { printError } from 'output/error';

export const run = async (argv: readonly string[]): Promise<void> => {
  try {
    const filePath = argv[argv.length - 1];

    await printCommissions(filePath);
  } catch (error) {
    printError(error);
  }
};

run(process.argv);
