import {
  CashInConfig,
  JuridicalCashOutConfig,
  NaturalCashOutConfig,
} from 'modules/config';
import { Operation, OperationType } from 'modules/operation';
import { UserType } from 'modules/user';
import * as commissionModule from '..';

const operations: readonly Operation[] = [
  {
    date: '2016-01-05',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashIn,
    operation: { amount: 200.0, currency: 'EUR' },
  },
  {
    date: '2016-01-06',
    userId: 2,
    userType: UserType.Juridical,
    type: OperationType.CashOut,
    operation: { amount: 300.0, currency: 'EUR' },
  },
  {
    date: '2016-01-06',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 30000, currency: 'EUR' },
  },
  {
    date: '2016-01-07',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 1000.0, currency: 'EUR' },
  },
  {
    date: '2016-01-07',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 100.0, currency: 'EUR' },
  },
  {
    date: '2016-01-10',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 100.0, currency: 'EUR' },
  },
  {
    date: '2016-01-10',
    userId: 2,
    userType: UserType.Juridical,
    type: OperationType.CashIn,
    operation: { amount: 1000000.0, currency: 'EUR' },
  },
  {
    date: '2016-01-10',
    userId: 3,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 1000.0, currency: 'EUR' },
  },
  {
    date: '2016-02-15',
    userId: 1,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 300.0, currency: 'EUR' },
  },
  {
    date: '2016-02-16',
    userId: 4,
    userType: UserType.Juridical,
    type: OperationType.CashOut,
    operation: { amount: 1, currency: 'EUR' },
  },
  {
    date: '2016-02-16',
    userId: 5,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 10, currency: 'EUR' },
  },
  {
    date: '2016-02-17',
    userId: 5,
    userType: UserType.Natural,
    type: OperationType.CashOut,
    operation: { amount: 3000, currency: 'EUR' },
  },
];

const cashInConfig: CashInConfig = {
  percents: 0.03,
  max: {
    amount: 5,
    currency: 'EUR',
  },
};

const naturalCashOutConfig: NaturalCashOutConfig = {
  percents: 0.3,
  weekLimit: {
    amount: 1000,
    currency: 'EUR',
  },
};

const juridicalCashOutConfig: JuridicalCashOutConfig = {
  percents: 0.3,
  min: {
    amount: 0.5,
    currency: 'EUR',
  },
};

describe('cash in', () => {
  it('gets cash in commission', () => {
    expect(
      commissionModule.getCashInCommission(operations[0], cashInConfig)
    ).toBeCloseTo(0.06);
  });

  it('gets cash in max commission', () => {
    expect(
      commissionModule.getCashInCommission(operations[6], cashInConfig)
    ).toBe(cashInConfig.max.amount);
  });
});

describe('natural cash out', () => {
  it('sorted by date operations returns same commission as not sorted operations', () => {
    const commission = 6.03;
    const operation = operations[operations.length - 1];
    const reversedOperations = [...operations].reverse();

    expect(
      commissionModule.getNaturalCashOutCommission(
        operations.indexOf(operation),
        operations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(commission);

    expect(
      commissionModule.getNaturalCashOutCommission(
        reversedOperations.indexOf(operation),
        reversedOperations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(commission);
  });

  it('gets natural cash out commission and exceeds week limit during first cash out', () => {
    expect(
      commissionModule.getNaturalCashOutCommission(
        2,
        operations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(87);
  });

  it('gets natural cash out commission and exceeds week limit during subsequent cash out', () => {
    expect(
      commissionModule.getNaturalCashOutCommission(
        11,
        operations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(6.03);
  });

  it('gets natural cash out commission with already exceeded week limit', () => {
    expect(
      commissionModule.getNaturalCashOutCommission(
        3,
        operations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(3);
  });

  it('gets natural cash out commission and does not exceed limit', () => {
    expect(
      commissionModule.getNaturalCashOutCommission(
        7,
        operations,
        naturalCashOutConfig
      )
    ).toBeCloseTo(0);
  });
});

describe('juridical cash out', () => {
  it('gets juridical cash out commission and exceeding minimum limit', () => {
    expect(
      commissionModule.getJuridicalCashOutCommission(
        operations[1],
        juridicalCashOutConfig
      )
    ).toBeCloseTo(0.9);
  });

  it('gets juridical cash out commission and does not exceed minimum limit', () => {
    expect(
      commissionModule.getJuridicalCashOutCommission(
        operations[9],
        juridicalCashOutConfig
      )
    ).toBeCloseTo(0.5);
  });
});

describe('get commissions', () => {
  it('gets commissions', () => {
    spyOn(commissionModule, 'getCashInCommission').and.returnValue(1);
    spyOn(commissionModule, 'getNaturalCashOutCommission').and.returnValue(2);
    spyOn(commissionModule, 'getJuridicalCashOutCommission').and.returnValue(3);

    expect(
      commissionModule.getCommissions(operations, {
        cashInConfig,
        naturalCashOutConfig,
        juridicalCashOutConfig,
      })
    ).toEqual([1, 3, 2, 2, 2, 2, 1, 2, 2, 3, 2, 2]);
  });

  it('throws error if wrong operation type is passed', () => {
    expect(() => {
      commissionModule.getCommissions(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        [{ ...operations[0], type: 'not_existing_type' as any }],
        {
          cashInConfig,
          naturalCashOutConfig,
          juridicalCashOutConfig,
        }
      );
    }).toThrow();
  });
});
