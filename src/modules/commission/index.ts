import {
  CashInConfig,
  JuridicalCashOutConfig,
  NaturalCashOutConfig,
} from 'modules/config';
import {
  areOperationsSortedByDate,
  Operation,
  OperationType,
  sortOperationsByDate,
} from 'modules/operation';
import { UserType } from 'modules/user';
import moment from 'moment';

export const getCashInCommission = (
  operation: Operation,
  config: CashInConfig
): number => {
  let commission = (operation.operation.amount / 100) * config.percents;

  if (commission > config.max.amount) {
    commission = config.max.amount;
  }

  return commission;
};

/**
 * Gets amount of money that were cashed out during the week until current operation
 * @param index current operation index
 * @param operations all operations (for performance reasons operations should be ordered by date)
 */
const getNaturalCashOutWeekAmount = (
  index: number,
  operations: readonly Operation[]
): number => {
  const operation = operations[index];
  const areOperationsSorted = areOperationsSortedByDate(operations);

  const sortedOperations = areOperationsSorted
    ? operations
    : sortOperationsByDate(operations);

  const indexInSortedOperations = areOperationsSorted
    ? index
    : sortedOperations.indexOf(operation);

  const weekNumber = moment(operation.date).isoWeek();
  let amount = 0;

  for (let idx = indexInSortedOperations - 1; idx >= 0; idx -= 1) {
    const prevOperation = sortedOperations[idx];

    if (weekNumber !== moment(prevOperation.date).isoWeek()) break;

    if (
      prevOperation.type === OperationType.CashOut &&
      prevOperation.userId === operation.userId
    ) {
      amount += prevOperation.operation.amount;
    }
  }

  return amount;
};

export const getNaturalCashOutCommission = (
  index: number,
  operations: readonly Operation[],
  config: NaturalCashOutConfig
): number => {
  const operation = operations[index];
  const currentWeekAmount = getNaturalCashOutWeekAmount(index, operations);
  const freeWeekAmountLeft =
    config.weekLimit.amount > currentWeekAmount
      ? config.weekLimit.amount - currentWeekAmount
      : 0;

  if (freeWeekAmountLeft >= operation.operation.amount) {
    return 0;
  }

  const taxableAmount = operation.operation.amount - freeWeekAmountLeft;

  return (taxableAmount / 100) * config.percents;
};

export const getJuridicalCashOutCommission = (
  operation: Operation,
  config: JuridicalCashOutConfig
): number => {
  let commission = (operation.operation.amount / 100) * config.percents;

  if (commission < config.min.amount) {
    commission = config.min.amount;
  }

  return commission;
};

export const getCommissions = (
  operations: readonly Operation[],
  config: {
    readonly cashInConfig: CashInConfig;
    readonly naturalCashOutConfig: NaturalCashOutConfig;
    readonly juridicalCashOutConfig: JuridicalCashOutConfig;
  }
): readonly number[] => {
  return operations.map((operation, index) => {
    if (operation.type === OperationType.CashIn) {
      return getCashInCommission(operation, config.cashInConfig);
    }

    if (
      operation.type === OperationType.CashOut &&
      operation.userType === UserType.Natural
    ) {
      return getNaturalCashOutCommission(
        index,
        operations,
        config.naturalCashOutConfig
      );
    }

    if (
      operation.type === OperationType.CashOut &&
      operation.userType === UserType.Juridical
    ) {
      return getJuridicalCashOutCommission(
        operation,
        config.juridicalCashOutConfig
      );
    }

    throw Error('Could not calculate commission for specified operation');
  });
};
