import AxiosMockAdapter from 'axios-mock-adapter';
import humps from 'humps';
import {
  api,
  CashInConfig,
  getCashInConfig,
  getJuridicalCashOutConfig,
  getNaturalCashOutConfig,
  JuridicalCashOutConfig,
  NaturalCashOutConfig,
} from '..';

const mockApi = new AxiosMockAdapter(api);

it('gets cash in configuration', async () => {
  const config: CashInConfig = {
    percents: 0.03,
    max: {
      amount: 5,
      currency: 'EUR',
    },
  };

  mockApi.onGet('config/cash-in').reply(200, config);

  await expect(getCashInConfig()).resolves.toEqual(config);
});

it('gets natural cash out configuration', async () => {
  const config: NaturalCashOutConfig = {
    percents: 0.3,
    weekLimit: {
      amount: 1000,
      currency: 'EUR',
    },
  };

  mockApi
    .onGet('config/cash-out/natural')
    .reply(200, humps.decamelizeKeys(config));

  await expect(getNaturalCashOutConfig()).resolves.toEqual(config);
});

it('gets juridical cash out configuration', async () => {
  const config: JuridicalCashOutConfig = {
    percents: 0.3,
    min: {
      amount: 0.5,
      currency: 'EUR',
    },
  };

  mockApi.onGet('config/cash-out/juridical').reply(200, config);

  await expect(getJuridicalCashOutConfig()).resolves.toEqual(config);
});
