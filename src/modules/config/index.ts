import axios from 'axios';
import humps from 'humps';
import { Price } from 'modules/price';

export interface CashInConfig {
  readonly percents: number;
  readonly max: Price;
}

export interface NaturalCashOutConfig {
  readonly percents: number;
  readonly weekLimit: Price;
}

export interface JuridicalCashOutConfig {
  readonly percents: number;
  readonly min: Price;
}

export const api = axios.create({
  baseURL: process.env.API_URL,
});

export const getCashInConfig = async (): Promise<CashInConfig> => {
  return (await api.get<CashInConfig>('config/cash-in')).data;
};

export const getNaturalCashOutConfig = async (): Promise<
  NaturalCashOutConfig
> => {
  return humps.camelizeKeys(
    (await api.get<NaturalCashOutConfig>('config/cash-out/natural')).data
  ) as NaturalCashOutConfig;
};

export const getJuridicalCashOutConfig = async (): Promise<
  JuridicalCashOutConfig
> => {
  return (await api.get<JuridicalCashOutConfig>('config/cash-out/juridical'))
    .data;
};
