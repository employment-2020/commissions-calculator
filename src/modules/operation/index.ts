import fs from 'fs';
import humps from 'humps';
import memoizeOne from 'memoize-one';
import { Price } from 'modules/price';
import { UserType } from 'modules/user';
import moment from 'moment';

export enum OperationType {
  CashIn = 'cash_in',
  CashOut = 'cash_out',
}

export interface Operation {
  readonly date: string;
  readonly userId: number;
  readonly userType: UserType;
  readonly type: OperationType;
  readonly operation: Price;
}

export const getOperationsFromFile = (
  filePath: string
): Promise<readonly Operation[]> => {
  return new Promise((resolve, reject) => {
    if (!filePath) {
      reject(new Error('Operations file path should not be empty'));
      return;
    }

    if (!filePath.endsWith('.json')) {
      reject(new Error('Operations file should end with .json extension'));
      return;
    }

    fs.readFile(filePath, (err, data) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(
        humps.camelizeKeys(
          JSON.parse(data.toString('utf8'))
        ) as readonly Operation[]
      );
    });
  });
};

export const areOperationsSortedByDate = memoizeOne(
  (operations: readonly Operation[]): boolean =>
    operations.every(
      (operation, index) =>
        index === 0 ||
        moment(operations[index - 1].date).isSameOrBefore(
          moment(operation.date)
        )
    )
);

export const sortOperationsByDate = memoizeOne(
  (operations: readonly Operation[]): readonly Operation[] =>
    [...operations].sort((a, b) => moment(a.date).diff(moment(b.date)))
);
