import fs from 'fs';
import { UserType } from 'modules/user';
import { mocked } from 'ts-jest/utils';
import {
  areOperationsSortedByDate,
  getOperationsFromFile,
  Operation,
  OperationType,
  sortOperationsByDate,
} from '..';

jest.mock('fs');

describe('get operations from file', () => {
  it('throws error when file path is empty', async () => {
    await expect(getOperationsFromFile('')).rejects.toThrow();
  });

  it('throws error when file is not .json', async () => {
    await expect(getOperationsFromFile('input.txt')).rejects.toThrow();
  });

  it('throws error that file could not be read', async () => {
    mocked(fs.readFile).mockImplementationOnce((_, callback) => {
      callback(new Error('File could not be read'), Buffer.from(''));
    });

    await expect(getOperationsFromFile('input.json')).rejects.toThrow();

    mocked(fs.readFile).mockReset();
  });

  it('returns operations from file', async () => {
    mocked(fs.readFile).mockImplementationOnce((_, callback) => {
      callback(
        null,
        Buffer.from(
          '[{ "date": "2016-01-05", "user_id": 1, "user_type": "natural", "type": "cash_in", "operation": { "amount": 200.00, "currency": "EUR" } }]'
        )
      );
    });

    const operations: readonly Operation[] = [
      {
        date: '2016-01-05',
        userId: 1,
        userType: UserType.Natural,
        type: OperationType.CashIn,
        operation: { amount: 200, currency: 'EUR' },
      },
    ];

    await expect(getOperationsFromFile('input.json')).resolves.toEqual(
      operations
    );

    mocked(fs.readFile).mockReset();
  });
});

it('checks if operations are sorted by date', () => {
  const operations: readonly Operation[] = [
    {
      date: '2016-01-05',
      userId: 1,
      userType: UserType.Natural,
      type: OperationType.CashIn,
      operation: { amount: 200, currency: 'EUR' },
    },
    {
      date: '2016-01-06',
      userId: 1,
      userType: UserType.Natural,
      type: OperationType.CashIn,
      operation: { amount: 200, currency: 'EUR' },
    },
  ];

  expect(areOperationsSortedByDate(operations)).toBe(true);
  expect(areOperationsSortedByDate([...operations].reverse())).toBe(false);
});

it('sorts operations by date', () => {
  const operations: readonly Operation[] = [
    {
      date: '2016-01-06',
      userId: 1,
      userType: UserType.Natural,
      type: OperationType.CashIn,
      operation: { amount: 200, currency: 'EUR' },
    },
    {
      date: '2016-01-05',
      userId: 1,
      userType: UserType.Natural,
      type: OperationType.CashIn,
      operation: { amount: 200, currency: 'EUR' },
    },
  ];

  expect(sortOperationsByDate(operations)).toEqual([...operations].reverse());
});
