export interface Price {
  readonly amount: number;
  readonly currency: 'EUR';
}
