import { getCommissions } from 'modules/commission';
import { mocked } from 'ts-jest/utils';
import { printCommissions } from '..';

jest.mock('modules/commission');
jest.mock('modules/config');
jest.mock('modules/operation');

it('checks if commissions are printed to console', async () => {
  const commissions = [1, 2.1, 3.22, 4.333];
  const consoleSpy = jest.spyOn(console, 'log');

  mocked(getCommissions).mockReturnValueOnce(commissions);

  await printCommissions('input.json');

  expect(consoleSpy).toHaveBeenCalledWith('1.00\n2.10\n3.22\n4.33');
});
