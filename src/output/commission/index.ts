import { getCommissions } from 'modules/commission';
import {
  getCashInConfig,
  getJuridicalCashOutConfig,
  getNaturalCashOutConfig,
} from 'modules/config';
import { getOperationsFromFile } from 'modules/operation';

export const printCommissions = async (
  operationsFilePath: string
): Promise<void> => {
  const [
    operations,
    cashInConfig,
    naturalCashOutConfig,
    juridicalCashOutConfig,
  ] = await Promise.all([
    getOperationsFromFile(operationsFilePath),
    getCashInConfig(),
    getNaturalCashOutConfig(),
    getJuridicalCashOutConfig(),
  ]);

  const commissions = getCommissions(operations, {
    cashInConfig,
    naturalCashOutConfig,
    juridicalCashOutConfig,
  });

  console.log(
    commissions.map((commission) => commission.toFixed(2)).join('\n')
  );
};
