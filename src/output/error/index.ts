export const printError = (error: Error): void => {
  console.error(error.message);
};
