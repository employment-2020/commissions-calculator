import { printError } from '..';

it('checks if error is printed to console', async () => {
  const consoleSpy = jest.spyOn(console, 'error');

  await printError(new Error('Error occured'));

  expect(consoleSpy).toHaveBeenCalled();
});
