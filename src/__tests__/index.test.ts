import { printCommissions } from 'output/commission';
import { printError } from 'output/error';
import { mocked } from 'ts-jest/utils';
import { run } from '..';

jest.mock('output/commission');
jest.mock('output/error');

it('checks if print commissions function is called with correct parameter', async () => {
  const argv = ['--arg1', '--arg2', '--arg3', 'input.json'];

  await run(argv);

  expect(printCommissions).toHaveBeenCalledWith(argv[argv.length - 1]);
});

it('checks if error printing function is called on error', async () => {
  const error = new Error('Failed to print commissions');

  mocked(printCommissions).mockRejectedValueOnce(error);

  await run([]);

  expect(printError).toHaveBeenCalledWith(error);
});
